#!/usr/bin/env python

import re, sys, datetime, os.path
from datetime import time, date, timedelta

# Delay in seconds to add to each end timecode (necessary to fix jumpiness)
delay = 1

def usage():
    print 'Usage: ' + sys.argv[0] + ' [filename].srt'
    sys.exit()

def file_warning():
    print "Please provide an SRT file.\n"
    usage()

# Input filename
if len(sys.argv) > 1:
    input_file = sys.argv[1]
    if input_file[-4:] == ".srt":
        input_file = sys.argv[1]
    else:
        file_warning()
else:
    file_warning()

# Output filename
output_file = input_file[:-4] + "_clean" + input_file[-4:]
prompt = "The file '{0}' already exists. Do you wish to overwrite? Enter y or n: ".format(output_file)

if os.path.isfile(output_file):
    response = raw_input(prompt)
    response = response.lower()
    if response == "y" or response == "yes":
        pass
    else:
        sys.exit()

## Cleanup part 1: swap in commas and set 1-hour start to zero-hour
regex = re.compile('\d\d:\d\d:\d\d\:\d\d\d')

with open(input_file, 'r') as f:
    mystring = f.read()

allmatches = re.findall(regex, mystring)
mydict = {}

# Get initial timecode values
init_timecode = re.search(r'(\d\d):(\d\d):(\d\d):',allmatches[0])
starthour = int(init_timecode.group(1))
startminute = int(init_timecode.group(2))
startsecond = int(init_timecode.group(3))

for time in allmatches:
    timecode = datetime.datetime.strptime(time, "%H:%M:%S:%f")
    starttime = datetime.timedelta(days=0,hours=starthour,minutes=startminute,seconds=startsecond,milliseconds=0)
    newtime = timecode - starttime
    newformatted = newtime.strftime("%H:%M:%S,%f")[:-3]
    mydict[time] = time.replace(time, newformatted)

for k, v in mydict.iteritems():
    # This var now contains all of the cleaned up content thus far
    mystring = mystring.replace(k, v)

## Cleanup part 2: add a delay to each end time
regex2 = re.compile('(\d\d:\d\d:\d\d,\d\d\d) --> (\d\d:\d\d:\d\d,\d\d\d)')

allmatches2 = re.findall(regex2, mystring)
mydict2 = {}

for index, element in enumerate(allmatches2):
    end = element[1]
    next_start = allmatches2[(index + 1) % len(allmatches2)][0]
    end_time = datetime.datetime.strptime(end, "%H:%M:%S,%f")
    next_start_time = datetime.datetime.strptime(next_start, "%H:%M:%S,%f")

    plus_one_sec = datetime.timedelta(days=0,hours=0,minutes=0,seconds=delay,milliseconds=0)

    # If adding the delay would make end time more than the following start time, just make it equal to the following start time
    if end_time + plus_one_sec > next_start_time:
        end_time = next_start_time
    else:
        end_time = end_time + plus_one_sec

    new_end_string = str(end_time.strftime("%H:%M:%S,%f")[:-3])
    mydict2[end] = end.replace(end, new_end_string)

for k, v in mydict2.iteritems():
    # This var now contains all of the final cleaned up content
    mystring = mystring.replace(k, v)

# Write to file
with open(output_file, "w") as f:
    f.write(mystring)
    print("Success! File saved as: {0}".format(output_file))
